```bash
# install vendors
composer install --ignore-platform-reqs

# run docker containers, make sure that ports 9990-9992
docker-compose up -d

# exec into container and run command in it
docker exec -ti poc-php /bin/sh

# run db migrations
bin/console d:m:m

# run fixtures
bin/console d:f:load
```

Adminer is available at `localhost:9990`:

```
server: poc-db
user: root
password: root
```

Playground is available on `http://localhost:9991/graphiql`

Try query for suggestedOrders:

```
{
  orders {
    productId
    warehouseId
    abcId
    date
    price
    amount
  }
}
```

There is working autocomplete for queries and mutations.

Database 3306 port is exposed to `localhost:9992` if you don't love Adminer.