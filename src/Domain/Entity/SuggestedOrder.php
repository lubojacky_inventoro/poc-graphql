<?php

namespace App\Domain\Entity;

use App\Domain\Entity\Interfaces\SuggestedOrderInterface;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\OrmRepository\SuggestedOrderRepository;

/**
 * @ORM\Entity(repositoryClass=SuggestedOrderRepository::class)
 */
class SuggestedOrder implements SuggestedOrderInterface
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", nullable=false)
     */
    private string $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $productId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $warehouseId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $abcId;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private DateTimeImmutable $date;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     */
    private float $price;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     */
    private float $amount;

    public function __construct(
        string $id,
        string $productId,
        string $warehouseId,
        string $abcId,
        DateTimeImmutable $date,
        float $price,
        float $amount
    )
    {
        $this->id = $id;
        $this->productId = $productId;
        $this->warehouseId = $warehouseId;
        $this->abcId = $abcId;
        $this->date = $date;
        $this->price = $price;
        $this->amount = $amount;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getWarehouseId(): string
    {
        return $this->warehouseId;
    }

    public function getAbcId(): string
    {
        return $this->abcId;
    }

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }
}