<?php


namespace App\Domain\Entity\Factory;


use App\Domain\Entity\SuggestedOrder;
use DateTimeImmutable;
use Ramsey\Uuid\Uuid;

class SuggestedOrderFactory
{
    public static function create(
        string $id,
        string $productId,
        string $warehouseId,
        string $abcId,
        DateTimeImmutable $date,
        float $price,
        float $amount
    ): SuggestedOrder
    {
        return new SuggestedOrder(
            $id, $productId, $warehouseId, $abcId, $date, $price, $amount
        );
    }

    public static function createRandom(): SuggestedOrder
    {
        $id = (Uuid::uuid4())->toString();
        $productId = (Uuid::uuid4())->toString();
        $warehouseId = (Uuid::uuid4())->toString();
        $abcId = (Uuid::uuid4())->toString();
        $date = new DateTimeImmutable('+'.rand(0,10).' days');
        $price = round(rand(10,100000)/100, 2);
        $amount = rand(1,100);

        return self::create($id, $productId, $warehouseId, $abcId, $date, $price, $amount);
    }
}