<?php

namespace App\Domain\Entity\Interfaces;

use DateTimeImmutable;

interface SuggestedOrderInterface
{
    public function getProductId(): string;

    public function getWarehouseId(): string;

    public function getAbcId(): string;

    public function getDate(): DateTimeImmutable;

    public function getPrice(): float;

    public function getAmount(): float;
}