<?php

namespace App\Domain\Repository;

use App\Domain\Entity\SuggestedOrder;

interface SuggestedOrderRepositoryInterface
{
    /**
     * @return SuggestedOrder[]
     */
    public function findAllOrders(): array;
}