<?php

namespace App\Graphql\Controller;

use App\Domain\Entity\SuggestedOrder;
use App\Domain\Repository\SuggestedOrderRepositoryInterface;
use App\Graphql\Type\Factory\SuggestedOrderTypeFactory;
use App\Graphql\Type\SuggestedOrderType;
use TheCodingMachine\GraphQLite\Annotations\Query;

class TestController
{
    /**
     * @var SuggestedOrderRepositoryInterface
     */
    private SuggestedOrderRepositoryInterface $suggestedOrderRepository;

    public function __construct(SuggestedOrderRepositoryInterface $suggestedOrderRepository)
    {
        $this->suggestedOrderRepository = $suggestedOrderRepository;
    }

    /**
     * @return SuggestedOrderType[]
     */
    #[Query]
    public function getOrders(): array
    {
        $orders = $this->suggestedOrderRepository->findAllOrders();

        return array_map(function (SuggestedOrder $suggestedOrder) {
            return SuggestedOrderTypeFactory::createFromEntity($suggestedOrder);
        }, $orders);
    }
}