<?php

namespace App\Graphql\Type;

use DateTimeImmutable;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

#[Type]
class SuggestedOrderType
{
    public function __construct(
        private string $productId,
        private string $warehouseId,
        private string $abcId,
        private DateTimeImmutable $date,
        private float $price,
        private float $amount
    )
    {

    }

    #[Field]
    public function getProductId(): string
    {
        return $this->productId;
    }

    #[Field]
    public function getWarehouseId(): string
    {
        return $this->warehouseId;
    }

    #[Field]
    public function getAbcId(): string
    {
        return $this->abcId;
    }

    #[Field]
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    #[Field]
    public function getPrice(): float
    {
        return $this->price;
    }

    #[Field]
    public function getAmount(): float
    {
        return $this->amount;
    }
}