<?php


namespace App\Graphql\Type\Factory;


use App\Domain\Entity\Interfaces\SuggestedOrderInterface;
use App\Graphql\Type\SuggestedOrderType;
use DateTimeImmutable;

class SuggestedOrderTypeFactory
{
    public static function create(
        string $productId,
        string $warehouseId,
        string $abcId,
        DateTimeImmutable $date,
        float $price,
        float $amount
    ): SuggestedOrderType
    {
        return new SuggestedOrderType(
            $productId,
            $warehouseId,
            $abcId,
            $date,
            $price,
            $amount
        );
    }

    public static function createFromEntity(SuggestedOrderInterface $suggestedOrder): SuggestedOrderType
    {
        return self::create(
            $suggestedOrder->getProductId(),
            $suggestedOrder->getWarehouseId(),
            $suggestedOrder->getAbcId(),
            $suggestedOrder->getDate(),
            $suggestedOrder->getPrice(),
            $suggestedOrder->getAmount(),
        );
    }
}