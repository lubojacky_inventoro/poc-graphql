<?php

namespace App\DataFixtures;

use App\Domain\Entity\Factory\SuggestedOrderFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SuggestedOrderFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $suggestedOrder = SuggestedOrderFactory::createRandom();
            $manager->persist($suggestedOrder);
        }

        $manager->flush();
    }
}
