<?php

namespace App\Infrastructure\OrmRepository;

use App\Domain\Entity\SuggestedOrder;
use App\Domain\Repository\SuggestedOrderRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SuggestedOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuggestedOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuggestedOrder[]    findAll()
 * @method SuggestedOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuggestedOrderRepository extends ServiceEntityRepository implements SuggestedOrderRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SuggestedOrder::class);
    }

    /**
     * @return SuggestedOrder[]
     */
    public function findAllOrders(): array
    {
        return $this->findBy([]);
    }
}